![alt text](assets/images/logo.png "TrackerWars")

# TrackerWars is online privacy promoting game.

This game never ends as fight against Internet trackers never ends either.
Note: If you wanna play the game on your computer make sure you've got
either Python 3.6 or 3.7 installed. 3.8 isn't supported by Kivy.
You can also use Python 3.5 but replace all f-strings with .format()
The point of the game is to protect your (An)droid (who's faithfully following you)
from trackers for as long as you can.

execute `buildozer android deploy run`

Have fun ...

technologies used:  Kivy (Python), Python For Android
