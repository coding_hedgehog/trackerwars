from kivy.app import App
from kivy.uix.widget import Widget
from kivy.core.window import Window
from kivy.core.audio import SoundLoader
from kivy.graphics import Rectangle, Ellipse
from kivy.uix.videoplayer import VideoPlayer
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.clock import Clock
from random import randint, choice
from kivy.config import Config
Config.set('kivy', 'window_icon', 'assets/images/logo.png')

#  Not using atlas loading as it stopped working for some reason.
#  Loading images individually. Not ideal but have no choice.

BULLETS = ['amazon_bullet.png', 'fb_bullet.png', 'g_bullet.png',
           'twitter_bullet.png']
SPARKLES = [f'{str(sparkle)}.png' for sparkle in range(1, 5)]
DEATHS = [f'{str(death)}.ogg' for death in range(1, 7)]
STAINS = [f'{stain}.png' for stain in range(1, 5)]
JOYSTICK = False
DEFLECTING_1 = False
DEFLECTING_2 = False
DEFLECTING_3 = False
USING_FORCE = False


class Background(Widget):

    def __init__(self, source):
        super(Background, self).__init__()
        self.image = Sprite(source=source)
        self.add_widget(self.image)
        self.size = self.image.size
        self.image_copy = Sprite(source=source, x=self.width)
        self.add_widget(self.image_copy)

    def update(self, dt):
        self.image.x -= 2
        self.image_copy.x -= 2

        if self.image_copy.x <= 0:
            self.image.x = self.x
            self.image_copy.x = self.width


class Sprite(Image):

    def __init__(self, **kwargs):
        super(Sprite, self).__init__(**kwargs)
        self.size = self.texture_size


class Stain(Sprite):

    def __init__(self, trooper_no, pos):
        super(Stain, self).__init__(
            source=f'assets/images/blood/{choice(STAINS)}', pos=pos)
        self.opacity = 0
        self.switch = False
        self.trooper_no = trooper_no

    def stain_goes_off(self, *ignore):
        self.opacity = 0
        self.switch = False

    def update(self, *ignore):
        if self.switch:
            Clock.schedule_once(self.stain_goes_off, 0.5)


class JarJarBinks(Sprite):

    def __init__(self, pos):
        super(JarJarBinks, self).__init__(
            source='assets/jar_jar_binks/1.png', pos=pos)
        self._walk_switch = False
        self.dropping_ammo = False
        self._step = 1

    def make_a_step(self, step):
        self.source = f'assets/jar_jar_binks/{step}.png'

    def update(self, dt):
        if self._walk_switch:
            self._step -= 1
            self.make_a_step(self._step)
            if self._step == 1:
                self._walk_switch = False
        elif not self._walk_switch:
            self._step += 1
            self.make_a_step(self._step)
            if self._step == 4:
                self._walk_switch = True
        x, y = self.pos
        x -= 10
        self.pos = (x, y)


class Clone(Sprite):

    def __init__(self, troop_id, pos):
        super(Clone, self).__init__(
            source='assets/troop/0.png', pos=pos)
        self.troop_id = troop_id
        self._temporarily_died = False
        self._death_step = 0
        self._blown_away_step = 0
        self._start_blow_away_anim = False
        self._switch = False
        self._step = 0
        self._opacity_step = 0.9

    def _make_a_blown_away_step(self, step):
        self.source = f'assets/troop/blown_away_animation/{step}.png'
        x, y = self.pos
        if self._blown_away_step > 3:
            x += 20
        else:
            x += 10
        y -= 2
        self.pos = (x, y)
        if step == 5:
            self._start_blow_away_anim = False

    def _make_next_death_step(self, step):
        # here I had to handle different image extension. Couldn't convert it
        # into PNG
        if step == 2:
            self.source = f'assets/troop/dying_animation/{step}.gif'
        else:
            self.source = f'assets/troop/dying_animation/{step}.png'
        if self._death_step == 6:
            y = self.pos[1]
            y -= 20
            self.pos = (self.pos[0], y)

    def update(self, dt):
        if USING_FORCE and self._start_blow_away_anim:
            if self._blown_away_step != 5:
                self._blown_away_step += 1
                self._make_a_blown_away_step(self._blown_away_step)
                self._temporarily_died = True

        if not self._temporarily_died:
            self.x -= 4
            if self._switch:
                self._step -= 1
                self._make_a_step(self._step)
                if self._step == 0:
                    self._switch = False
            elif not self._switch:
                self._step += 1
                self._make_a_step(self._step)
                if self._step == 2:
                    self._switch = True

        if self._temporarily_died and self._blown_away_step == 0:
            self._death_step += 1
            if self._death_step > 6:
                pass
            else:
                self._make_next_death_step(self._death_step)
        if self._temporarily_died and \
                self.source == 'assets/troop/dying_animation/6.png' or \
                self._temporarily_died and \
                self.source == 'assets/troop/blown_away_animation/5.png':
            x, y = self.pos
            x -= 2
            self.pos = (x, y)
            self.opacity = self._opacity_step
            self._opacity_step -= .01
            if self.opacity < 0:
                self._blown_away_step = 0
                self._death_step = 0
                self._opacity_step = 0.9
                self.opacity = 1
                self._temporarily_died = False
                self.pos = (randint(700, 800), randint(1, 200))

    def _make_a_step(self, step):
        self.source = f'assets/troop/{step}.png'


class Bullet(Sprite):

    def __init__(self, trooper_no, pos):
        super(Bullet, self).__init__(
            source=f'assets/bullets/{choice(BULLETS)}', pos=pos)
        self.trooper_no = trooper_no
        self.deflected = False
        self.deflected_and_heading_up = False

    def update(self, *ignore):
        if self.deflected:
            x = self.pos[0]
            y = self.pos[1]
            x += 20
            if self.deflected_and_heading_up:
                y += 4
            else:
                y -= 4
            self.pos = (x, y)
        else:
            x = self.pos[0]
            y = self.pos[1]
            x -= 10
            self.pos = (x, y)


class Jedi(Sprite):

    def __init__(self, pos):
        super(Jedi, self).__init__(
            #  source='atlas://assets/jedi/light_saber_anim/none', pos=pos)
            source='assets/jedi/lightsaber_animation/0.png', pos=pos)
        self.lightsaber_on = SoundLoader.load(
            'assets/sounds/lightsaber_on.ogg')
        self.lightsaber_off = SoundLoader.load(
            'assets/sounds/lightsaber_off.ogg')
        self._step = 0
        self._walk_switch = False
        self.pick_waving_sound = [1, 3, 5, 6, 7, 8]
        self.pick_conflicting_sound = [2, 4, 9, 10]
        Clock.schedule_once(self._turn_on_lightsaber, 2)

    def _make_a_step(self, step):
        self.source = f'assets/jedi/jedi_walk_animation/{step}.png'

    def _use_force(self, dt):
        self.source = 'assets/jedi/using_force.png'
        Clock.schedule_once(self._carry_on_jedi, 2)

    def _carry_on_jedi(self, *ignore):
        global USING_FORCE
        USING_FORCE = False

    def _turn_on_lightsaber(self, *ignore):
        for phase in range(0, 7):
            self.source = f'assets/jedi/lightsaber_animation/{phase}.png'
        self.lightsaber_on.play()

    def _turn_off_lightsaber(self, *ignore):
        for phase in reversed(range(0, 7)):
            self.source = f'assets/jedi/lightsaber_animation/{phase}.png'
        self.lightsaber_off.play()
        Clock.schedule_once(self._use_force, 0)

    def update(self, dt):
        if self._walk_switch:
            self._step -= 1
            self._make_a_step(self._step)
            if self._step == 0:
                self._walk_switch = False
        elif not self._walk_switch:
            self._step += 1
            self._make_a_step(self._step)
            if self._step == 3:
                self._walk_switch = True

    def play_conflicting_sound(self):
        sound = choice(self.pick_conflicting_sound)
        sound_loaded = SoundLoader.load(f'assets/sounds/{str(sound)}.ogg')
        sound_loaded.play()

    def play_waving_sound(self):
        sound = choice(self.pick_waving_sound)
        sound_loaded = SoundLoader.load(f'assets/sounds/{str(sound)}.ogg')
        sound_loaded.play()

    def deflect_1(self, hitting=False):
        global DEFLECTED
        global DEFLECTING_1
        DEFLECTING_1 = True
        if not hitting:
            self.play_waving_sound()
        else:
            self.play_conflicting_sound()
        self.source = 'assets/jedi/jedi_deflecting.png'

    def deflect_2(self, hitting=False):
        global DEFLECTED
        global DEFLECTING_2
        DEFLECTING_2 = True
        if not hitting:
            self.play_waving_sound()
        else:
            self.play_conflicting_sound()
        self.source = 'assets/jedi/jedi_deflecting_2.png'

    def deflect_3(self, hitting=False):
        global DEFLECTED
        global DEFLECTING_3
        DEFLECTING_3 = True
        if not hitting:
            self.play_waving_sound()
        else:
            self.play_conflicting_sound()
        self.source = 'assets/jedi/jedi_deflecting_3.png'

    def using_force(self):
        global USING_FORCE
        USING_FORCE = True
        Clock.schedule_once(self._turn_off_lightsaber, 2)


class Game(Widget):

    def __init__(self):
        super(Game, self).__init__()
        self.background = Background(
            source='assets/images/bg.jpg')
        self.size = self.background.size

        # (un)comment below 2 lines for development / deployment
        # self.play_intro()
        self.run_game_init()

    def run_game_init(self):
        self.yoda = Sprite(source='assets/images/yoda.png', pos=(
            self.background.width/2-179, self.background.height/2-139.5))
        self.the_four = Sprite(source='assets/images/the_four.png', pos=(
            self.background.width/2-163, (self.background.height/2-163)-20))
        self.troops = [Clone(troop_id=x, pos=(
            randint(self.background.width, self.background.width+300),
            randint(1, 200))) for x in range(1, 8)]
        self.bullets = [Bullet(trooper_no=troop.troop_id, pos=(
            troop.pos[0]-15, troop.pos[1]+80)) for troop in self.troops]
        self.stains = [Stain(trooper_no=troop.troop_id, pos=(
            troop.pos[0]+25, troop.pos[1]+50)) for troop in self.troops]
        self.add_widget(self.background)

        self.btn_off = 'assets/images/btn_off.jpg'
        self.btn_on = 'assets/images/btn_on.jpg'
        self.red_indicator = 'assets/images/red.jpg'
        self.blue_indicator = 'assets/images/blue.jpg'
        self.dropping = False
        for troop in self.troops:
            self.add_widget(troop)
        for stain in self.stains:
            self.add_widget(stain)
        for bullet in self.bullets:
            self.add_widget(bullet)
        self.music = SoundLoader.load('assets/sounds/music.ogg')
        self.droid_hit = SoundLoader.load('assets/sounds/r2d2_hit.ogg')
        self.R2D2_informing = SoundLoader.load(
            'assets/sounds/R2D2_informs_of_force_enabled.ogg')
        self.music.play()
        self.player = Jedi(pos=(130, self.background.height / 2 - 50))
        self.waves = Sprite(source='assets/images/waves.png',
                            pos=(self.player.pos[0]+90, self.player.pos[1] + 20))
        self.waves.opacity = 0
        self.droid = Sprite(source='assets/r2d2/r2d2.png',
                            pos=(self.player.pos[0]-30, self.player.pos[1]+10))
        self.droid.hits = 0
        self.droid_was_hit = False
        self.sparkle = Sprite(source='assets/sparkles/1.png',
                              pos=(self.player.pos[0]+50, self.player.pos[1]+70))
        self.game_over1 = Sprite(source='assets/r2d2/r2d2_1.png', pos=(
            self.background.width/2-151.5, self.background.height/2-125))
        self.game_over2 = Sprite(source='assets/r2d2/r2d2_2.png', pos=(
            self.background.width/2-151.5, self.background.height/2-125))
        self.game_over1.opacity = 0
        self.game_over2.opacity = 0
        self.sparkle.opacity = 0
        self.jar_jar_binks = JarJarBinks(pos=(self.background.width, 1))
        self.duck_it_ammo = Sprite(source='assets/images/duck_it.png',
                                   pos=(self.jar_jar_binks.pos[0]+70,
                                        self.jar_jar_binks.pos[1]+20))
        self.duck_it_ammo.opacity = 0
        self.duck_it_ammo.dropped = False
        self.duck_it_ammo.enabled = False
        self.duck_it_ammo.follow_background = False
        self.assets = [self.player, self.waves, self.sparkle, self.droid,
                       self.duck_it_ammo, self.jar_jar_binks, self.the_four]
        for asset in self.assets:
            self.add_widget(asset)
        self.indicator_height = 40
        self.indicator_width = (self.background.width - 100) / 5
        self.location_label_pos = 0
        self.browsing_history_label_pos = 0
        self.shopping_habits_label_pos = 0
        self.health_condition_label_pos = 0
        self.emails_label_pos = 0

        # creates buttons, status bar, and touchpad with joystick
        with self.canvas:
            self.btn1 = Rectangle(source=self.btn_off,
                                  pos=(self.background.width-100, 0), size=(
                                      100, 100))
            self.btn2 = Rectangle(source=self.btn_off,
                                  pos=(self.background.width-100, 101),
                                  size=(100, 100))
            self.btn3 = Rectangle(source=self.btn_off,
                                  pos=(self.background.width-100, 201),
                                  size=(100, 100))
            self.duck_it = Rectangle(source='assets/images/duck_it_disabled.jpg',
                                     pos=(self.background.width-100,
                                          self.btn3.pos[1]+101),
                                     size=(100, self.background.height-300))
            self.touchpad = Rectangle(source='assets/images/touchpad.jpg',
                                      pos=(0, 0), size=(
                                          100, self.background.height - 50))

            self.joystick = Ellipse(source='assets/images/joystick.png',
                                    pos=(self.player.pos[0]-100,
                                         self.player.pos[1]+20), size=(
                                        40, 40))

            self.location = Rectangle(source=self.blue_indicator, pos=(
                0, self.background.height - self.indicator_height),
                size=(self.indicator_width, self.indicator_height))

            self.browsing_history = Rectangle(source=self.blue_indicator, pos=(
                self.location.pos[0]+self.indicator_width, self.location.pos[1]),
                size=(self.indicator_width, self.indicator_height))
            self.shopping_habits = Rectangle(source=self.blue_indicator,  pos=(
                self.browsing_history.pos[0]+self.indicator_width,
                self.location.pos[1]), size=(self.indicator_width,
                                             self.indicator_height))
            self.health_condition = Rectangle(source=self.blue_indicator, pos=(
                self.shopping_habits.pos[0]+self.indicator_width,
                self.location.pos[1]), size=(self.indicator_width,
                                             self.indicator_height))
            self.emails = Rectangle(source=self.blue_indicator, pos=(
                self.health_condition.pos[0]+self.indicator_width,
                self.location.pos[1]), size=(self.indicator_width,
                                             self.indicator_height))

        self.location_label = Label(
            text='LOCATION', pos=(
                self.location.pos[0]+12, self.location.pos[1]-30))
        self.browsing_history_label = Label(
            text='BROWSING\n  HISTORY', font_size='10sp', pos=(
                self.browsing_history.pos[0]+12,
                self.browsing_history.pos[1]-30))
        self.shopping_habits_label = Label(
            text='  SHOPPING\n     HABITS', font_size='10sp', pos=(
                self.shopping_habits.pos[0]+12,
                self.shopping_habits.pos[1]-30))
        self.health_condition_label = Label(
            text='  HEALTH\nCONDITION', font_size='10sp', pos=(
                self.health_condition.pos[0]+12,
                self.health_condition.pos[1]-30))
        self.emails_label = Label(
            text='EMAILS', pos=(self.emails.pos[0]+11, self.emails.pos[1]-30))
        self.assets2 = [self.location_label, self.browsing_history_label,
                        self.shopping_habits_label, self.health_condition_label,
                        self.emails_label, self.game_over1, self.game_over2]
        for asset in self.assets2:
            self.add_widget(asset)
        Clock.schedule_interval(self.check_the_four_screen_is_gone, 1.0/60.0)

    def play_intro(self):
        self.intro = VideoPlayer(source='assets/intro.avi', state='play',
                                 options={'allow_stretch': True})
        self.intro.size = (self.size[0], self.size[1])
        self.add_widget(self.intro)
        Clock.schedule_once(self.remove_intro, 60)

    def remove_intro(self, dt):
        self.remove_widget(self.intro)
        self.run_game_init()

    def check_the_four_screen_is_gone(self, dt):
        if self.the_four is None:
            Clock.unschedule(self.check_the_four_screen_is_gone)
            self.add_widget(self.yoda)
            Clock.schedule_once(self.start_moving, 5)

    def stop_clocks(self):
        Clock.unschedule(self.update_background)
        Clock.unschedule(self.update_player,)
        Clock.unschedule(self.update_waves,)
        Clock.unschedule(self.update_droid,)
        Clock.unschedule(self.update_troops,)
        Clock.unschedule(self.update_bullets)
        Clock.unschedule(self.update_stains)
        Clock.unschedule(self.update_sparkle)
        Clock.unschedule(self.update_hits)
        Clock.unschedule(self.update_jarjarbinks)
        self.game_over1.opacity = 1
        Clock.schedule_once(self.change_game_over_pictures, .5)

    def start_moving(self, dt):
        self.remove_widget(self.yoda)
        Clock.schedule_interval(self.update_background, 7.0/60.0)
        Clock.schedule_interval(self.update_player, 10.0/60.0)
        Clock.schedule_interval(self.update_waves, 10.0/60.0)
        Clock.schedule_interval(self.update_droid, 10.0/60.0)
        Clock.schedule_interval(self.update_troops, 10.0/60.0)
        Clock.schedule_interval(self.update_bullets, 1.5/60.0)
        Clock.schedule_interval(self.update_stains, 5.0/60.0)
        Clock.schedule_interval(self.update_sparkle, 5.0/60.0)
        Clock.schedule_interval(self.update_hits, 1.0/60.0)
        Clock.schedule_interval(self.update_jarjarbinks, 5.0/60.0)

    def change_game_over_pictures(self, dt):
        self.game_over1.opacity = 0
        self.game_over2.opacity = 1
        Clock.schedule_once(self.game_over, .5)

    def game_over(self, dt):
        self.music.stop()
        self.game_over2.opacity = 0
        self.game_over1.opacity = 1
        self.game_over_sound = SoundLoader.load('assets/sounds/game_over.ogg')
        self.game_over_sound.play()

    def drop_duck(self, dt):
        self.jar_jar_binks.source = 'assets/jar_jar_binks/dropping.png'
        self.duck_it_ammo.dropped = True
        self.duck_it_ammo.opacity = 1
        Clock.schedule_once(self.get_up_and_carry_on_jar_jar_binks, 1)

    def get_up_and_carry_on_jar_jar_binks(self, dt):
        self.jar_jar_binks.source = \
            f'assets/jar_jar_binks/{self.jar_jar_binks._step}.png'
        self.jar_jar_binks.dropping_ammo = False
        self.duck_it_ammo.follow_background = True

    def update_jarjarbinks(self, dt):
        if not self.jar_jar_binks.dropping_ammo:
            self.jar_jar_binks.update(dt)
            # check if Jar Jar Binks dropped the duck ammo, the ammo will
            # follow him until he does.
            if not self.duck_it_ammo.dropped:
                q, w = self.duck_it_ammo.pos
                q = self.jar_jar_binks.pos[0] + 50
                w = self.jar_jar_binks.pos[1] + 10
                self.duck_it_ammo.pos = (q, w)
            # making sure the duck ammo moves in the same speed as the background
            # when dropped on the ground.
            if self.duck_it_ammo and self.duck_it_ammo.follow_background:
                q, w = self.duck_it_ammo.pos
                q -= 2
                self.duck_it_ammo.pos = (q, w)
            # Jar Jar Binks drops the duck ammo in specified spot.
            if self.jar_jar_binks.pos[0] < 400 and self.jar_jar_binks.pos[0] > \
                    350 and self.jar_jar_binks._step == 1:
                self.jar_jar_binks.dropping_ammo = True
            # making Jar Jar Binks's appearance little bit more unpredictible.
            if self.jar_jar_binks.pos[0] < 0 and self.duck_it_ammo.pos[0] < 0:
                self.duck_it_ammo.dropped = False
                self.duck_it_ammo.opacity = 0
                randomize_reapearance = True if randint(
                    0, 10) == 8 and randint(0, 10) == 0 else False
                if randomize_reapearance:
                    x = self.background.width
                    self.jar_jar_binks.pos = (x, 1)
                    self.duck_it_ammo.pos = \
                        (self.jar_jar_binks.pos[0], self.jar_jar_binks.pos[1])

        else:
            Clock.schedule_once(self.drop_duck, 1)

    def update_player(self, dt):
        if not any([DEFLECTING_1, DEFLECTING_2, DEFLECTING_3, USING_FORCE]):
            self.player.update(dt)
            # If player is near enough the duck force he will collect it
            # and R2D2 will make a sound
            if self.player.pos[0]+60 > self.duck_it_ammo.pos[0] and \
                    self.player.pos[0]+30 < self.duck_it_ammo.pos[0]+15 and \
                    self.player.pos[1] > 0 and self.player.pos[1] < \
                    self.duck_it_ammo.pos[1] + 20:
                self.duck_it.source = 'assets/images/duck_it_enabled.jpg'
                self.duck_it_ammo.opacity = 0
                self.duck_it_ammo.enabled = True
                self.R2D2_force_enabled()
        if self.player.source == 'assets/jedi/using_force.png':
            self.waves.opacity = 1

    def update_sparkle(self, dt):
        x, y = self.sparkle.pos
        x = self.player.pos[0]+50
        y = self.player.pos[1]+70
        self.sparkle.pos = (x, y)

    def update_waves(self, dt):
        x, y = self.waves.pos
        if self.waves.opacity == 0:
            x = self.player.pos[0]+90
            y = self.player.pos[1]+20
            self.waves.pos = (x, y)
        else:
            x += 10
            self.waves.pos = (x, y)
            if self.waves.pos[0] > self.background.width:
                self.waves.opacity = 0
            for troop in self.troops:
                troop._start_blow_away_anim = True

    def update_stains(self, dt):
        for stain in self.stains:
            stain.update(dt)
            x, y = stain.pos
            x = self.troops[stain.trooper_no-1].pos[0]+25
            y = self.troops[stain.trooper_no-1].pos[1]+50
            stain.pos = (x, y)

    def update_droid(self, dt):
        x, y = self.player.pos[0], self.player.pos[1]+10
        self.droid.pos = (x-30, y+3)
        self.droid.source = 'assets/r2d2/r2d2.png'
        if self.droid_was_hit:
            self.droid.hits += 1
            self.droid_was_hit = False

    def update_hits(self, dt):
        if self.droid.hits >= 5:
            self.location.source = self.red_indicator
        if self.droid.hits >= 10:
            self.browsing_history.source = self.red_indicator
        if self.droid.hits >= 20:
            self.shopping_habits.source = self.red_indicator
        if self.droid.hits >= 30:
            self.health_condition.source = self.red_indicator
        if self.droid.hits == 40:
            self.emails.source = self.red_indicator
            self.stop_clocks()

    def sparkle_goes_off(self, *ignore):
        self.sparkle.opacity = 0

    def play_troops_death_sound(self, *ignore):
        death_sound = SoundLoader.load(
            f'assets/sounds/deaths/{choice(DEATHS)}')
        death_sound.play()

    def R2D2_force_enabled(self):
        self._sound_switch = False
        self.R2D2_informing.play()

    def update_bullets(self, dt):
        for bullet in self.bullets:

            if bullet.pos[0] < 0:
                for troop in self.troops:
                    if bullet.trooper_no == troop.troop_id:
                        if not troop._temporarily_died:
                            bullet.pos = (troop.pos[0]-15, troop.pos[1]+85)
            # here I'm checking if flying "bullets" hit R2D2
            elif bullet.pos[0] < self.droid.pos[0]+33 and bullet.pos[0] > \
                    self.droid.pos[0] and bullet.pos[1] < \
                    self.droid.pos[1]+46 and bullet.pos[1] > self.droid.pos[1]:
                self.droid_was_hit = True
                self.droid.source = 'assets/r2d2/r2d2_hit.png'
                self.droid_hit.play()
            # Show sparkle for a second if bullet gets near player's lightsaber
            # and player is deflecting. Decide if deflected bullet goes up
            # or down
            elif bullet.pos[0] < self.player.pos[0]+91 and \
                bullet.pos[0] > self.player.pos[0] + 85 and \
                    bullet.pos[1] > self.player.pos[1] + 50 and \
                    bullet.pos[1] < self.player.pos[1] + 110 and DEFLECTING_1:
                self.player.deflect_1(hitting=True)
                self.sparkle.source = f'assets/sparkles/{choice(SPARKLES)}'
                self.sparkle.opacity = 1
                Clock.schedule_once(self.sparkle_goes_off, 0.5)
                bullet.deflected = True
                bullet.deflected_and_heading_up = True if \
                    randint(1, 8) % 2 == 0 else False

            elif bullet.pos[0] < self.player.pos[0]+91 and \
                bullet.pos[0] > self.player.pos[0] + 85 and \
                    bullet.pos[1] > self.player.pos[1] and \
                    bullet.pos[1] < self.player.pos[1] + 80 and DEFLECTING_2:
                self.player.deflect_2(hitting=True)
                self.sparkle.source = f'assets/sparkles/{choice(SPARKLES)}'
                self.sparkle.opacity = 1
                Clock.schedule_once(self.sparkle_goes_off, 0.5)
                bullet.deflected = True
                bullet.deflected_and_heading_up = True if \
                    randint(1, 8) % 2 == 0 else False
            elif bullet.pos[0] < self.player.pos[0]+92 and \
                    bullet.pos[0] > self.player.pos[0] + 75 and \
                    bullet.pos[1] > self.player.pos[1] + 55 and \
                    bullet.pos[1] < self.player.pos[1] + 110 and DEFLECTING_3:
                self.player.deflect_3(hitting=True)
                self.sparkle.source = f'assets/sparkles/{choice(SPARKLES)}'
                self.sparkle.opacity = 1
                Clock.schedule_once(self.sparkle_goes_off, 0.5)
                bullet.deflected = True
                bullet.deflected_and_heading_up = True if \
                    randint(1, 8) % 2 == 0 else False

            # here I'm checking if deflected bullets hit any troops
            for troop in self.troops:
                if bullet.pos[0] > troop.pos[0] and bullet.pos[0] < \
                        troop.pos[0]+10 and bullet.pos[1] > troop.pos[1]+20 and \
                        bullet.pos[1] < troop.pos[1]+123 and \
                        bullet.deflected and not troop._temporarily_died:
                    for stain in self.stains:
                        if stain.trooper_no == troop.troop_id and \
                                stain.opacity == 0:
                            stain.opacity = 1
                            stain.switch = True

                    self.play_troops_death_sound()
                    troop._temporarily_died = True
                    bullet.source = f'assets/bullets/{choice(BULLETS)}'
                    bullet.deflected = False
                    if not troop._temporarily_died:
                        bullet.pos = (troop.pos[0], troop.pos[1]+100)

                elif bullet.deflected and troop.troop_id == bullet.trooper_no:
                    if bullet.pos[0] > self.background.width or \
                            bullet.pos[0] < 0 or bullet.pos[1] > \
                            self.background.height or \
                            bullet.pos[1] < 0:
                        bullet.deflected = False
                        bullet.pos = (troop.pos[0]-15, troop.pos[1]+85)

            bullet.update(dt)

    def update_troops(self, dt):

        for troop in self.troops:
            troop.update(dt)
            if troop._blown_away_step == 2:
                Clock.schedule_once(self.play_troops_death_sound, 0)
            # if troop goes off the screen move him back to the beginning
            # and give him different bullet.
            elif troop.pos[0] < 0:
                troop.pos = (self.background.width, randint(1, 200))
                for bullet in self.bullets:
                    if bullet.trooper_no == troop.troop_id:
                        bullet.source = f'assets/bullets/{choice(BULLETS)}'
                        bullet.pos = (troop.pos[0], troop.pos[1]+100)
            # here I'm checking if troop is near Jedi and Jedi is fighting
            # if Jedi is fighting kill the troop and run animation.
            elif troop.pos[0] < self.player.pos[0]+50 and troop.pos[0] > \
                    self.player.pos[0] + 30 and troop.pos[1] > \
                    self.player.pos[1] - 20 and troop.pos[1] < \
                    self.player.pos[1]+30 and \
                    any([DEFLECTING_1, DEFLECTING_2, DEFLECTING_3]):
                self.play_troops_death_sound()
                troop._temporarily_died = True
                self.sparkle.opacity = 1
                for stain in self.stains:
                    if stain.trooper_no == troop.troop_id:
                        stain.opacity = 1
                        stain.switch = True
                Clock.schedule_once(self.sparkle_goes_off, )

            for stain in self.stains:
                if stain.trooper_no == troop.troop_id:
                    x, y = stain.pos
                    x = troop.pos[0]+25
                    stain.pos = (x, y)

    def update_background(self, dt):
        if not any([DEFLECTING_1, DEFLECTING_2, DEFLECTING_3, USING_FORCE]):
            self.background.update(dt)

    def on_touch_down(self, touch):
        # checking what user is tapping on in the interface and taking action.
        if touch.pos[0] > self.btn1.pos[0] and touch.pos[0] < \
            self.btn1.pos[0]+100 and \
                touch.pos[1] > 0 and touch.pos[1] < \
                self.btn1.size[0]:
            self.btn1.source = self.btn_on
            self.player.deflect_1()
        if self.the_four and touch.pos[0] > 498 and \
                touch.pos[0] < 513 and touch.pos[1] > 310 and \
                touch.pos[1] < 330:
            self.remove_widget(self.the_four)
            self.the_four = None
        if touch.pos[0] > self.btn2.pos[0] and touch.pos[0] < \
            self.btn2.pos[0]+100 and \
                touch.pos[1] > self.btn2.pos[1] and touch.pos[1] < \
                self.btn2.pos[1]+100:
            self.btn2.source = self.btn_on
            self.player.deflect_2()
        if touch.pos[0] > self.btn3.pos[0] and touch.pos[0] < \
                self.btn3.pos[0]+100 and touch.pos[1] > self.btn3.pos[1] and \
                touch.pos[1] < self.btn3.pos[1]+100:
            self.btn3.source = self.btn_on
            self.player.deflect_3()
        if touch.pos[0] > 0 and touch.pos[0] < self.touchpad.size[1] and \
            touch.pos[1] > 0 and touch.pos[1] < self.touchpad.size[1]-44 and \
                touch.pos[0] > self.joystick.pos[0]+14 and \
                touch.pos[0] < self.joystick.pos[0]+40 and \
                touch.pos[1] > self.joystick.pos[1]+14 and \
                touch.pos[1] < self.joystick.pos[1] + 40:
            global JOYSTICK
            JOYSTICK = True
        # If player taps on force button blow clones away
        if touch.pos[0] > self.duck_it.pos[0] and touch.pos[0] < \
                self.duck_it.pos[0]+100 and touch.pos[1] > \
                self.duck_it.pos[1] and touch.pos[1] < \
                self.duck_it.pos[1] + (self.background.height-300) and \
                self.duck_it_ammo.enabled:

            # not particularly happy about the below code but it does
            # compensate different scalings of sprites
            x, y = self.player.pos
            y -= 30
            self.player.pos = (x, y)
            self.player.using_force()
            x, y = self.player.pos
            y += 30
            self.player.pos = (x, y)
            # --------------------------------------------------------
            self.duck_it_ammo.enabled = False
            self.duck_it.source = self.duck_it.source = \
                'assets/images/duck_it_disabled.jpg'

    def on_touch_up(self, touch):
        global JOYSTICK
        global DEFLECTING_1
        global DEFLECTING_2
        global DEFLECTING_3
        global USING_FORCE
        JOYSTICK = False
        DEFLECTING_1 = False
        DEFLECTING_2 = False
        DEFLECTING_3 = False
        self.btn1.source = self.btn_off
        self.btn2.source = self.btn_off
        self.btn3.source = self.btn_off

    def on_touch_move(self, touch):
        # Here I'm making sure that joystick stays within the boundries
        # of the touchpad. It won't allow user move it beyond the boundries.
        if JOYSTICK and self.joystick.pos[0] < 0:
            touch.pos = (touch.pos[0]+13.5, touch.pos[1])
            self.joystick.pos = (touch.pos[0], touch.pos[1])
            self.player.pos = (self.joystick.pos[0]+110, self.joystick.pos[1])

        if JOYSTICK and touch.pos[0]+27 > self.touchpad.size[0]:
            touch.pos = (self.touchpad.size[0]-27, touch.pos[1])
            self.joystick.pos = (touch.pos[0], touch.pos[1])
            self.player.pos = (self.joystick.pos[0]+110, self.joystick.pos[1])

        if JOYSTICK and self.joystick.pos[1] < 0:
            touch.pos = (touch.pos[0], touch.pos[1]+27)
            self.joystick.pos = (touch.pos[0], touch.pos[1])
            self.player.pos = (self.joystick.pos[0]+110, self.joystick.pos[1])

        if JOYSTICK and touch.pos[1]+27 > self.touchpad.size[1]:
            touch.pos = (touch.pos[0], self.touchpad.size[1]-13.5)
            self.joystick.pos = (touch.pos[0], touch.pos[1])
            self.player.pos = (self.joystick.pos[0]+110, self.joystick.pos[1])

        if JOYSTICK:
            x = touch.pos[0]
            x -= 14
            y = touch.pos[1]
            y -= 27
            self.joystick.pos = (x, y)
            self.player.pos = (self.joystick.pos[0]+110, self.joystick.pos[1])


class TrackerWarsApp(App):

    def build(self):
        game = Game()
        Window.size = game.size
        return game


if __name__ == '__main__':
    TrackerWarsApp().run()
